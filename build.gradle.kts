plugins {
    kotlin("multiplatform") version "2.1.0"
}

group = "de.connect2x"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/47538655/packages/maven") // trixnity-messenger
}

kotlin {
    jvmToolchain(17)
    js {
        browser {
            commonWebpackConfig {
                configDirectory = rootDir.resolve("src/webpack.config.d")
            }
            runTask {
                mainOutputFileName = "web.js"
            }
            webpackTask {
                mainOutputFileName = "web.js"
            }
        }
        binaries.executable()
    }
    sourceSets {
        val jsMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-html-js:0.11.0")

                implementation(project.dependencies.enforcedPlatform("org.jetbrains.kotlin-wrappers:kotlin-wrappers-bom:2025.3.2"))
                implementation("org.jetbrains.kotlin-wrappers:kotlin-react")
                implementation("org.jetbrains.kotlin-wrappers:kotlin-react-dom")

                implementation("org.jetbrains.kotlin-wrappers:kotlin-mui-material:2025.1.3-5.16.13")
                implementation(npm("@mui/material", "5.16.13"))
                implementation(npm("@emotion/react", "11.11.4"))
                implementation(npm("@emotion/styled", "11.11.0"))

                implementation("de.connect2x:trixnity-messenger:3.4.2") // trixnity-messenger
                implementation("org.slf4j:slf4j-api:2.0.16") // logging in trixnity-messenger
                implementation("io.ktor:ktor-client-js:3.1.0") // HTTP client used in trixnity-messenger
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.9.0")

                implementation(npm("copy-webpack-plugin", "11.0.0"))
            }
        }
    }
}

