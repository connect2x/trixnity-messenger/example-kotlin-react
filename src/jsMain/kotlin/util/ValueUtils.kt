package de.connect2x.util

import com.arkivanov.decompose.value.Value
import react.StateInstance
import react.useEffectOnceWithCleanup
import react.useState

internal fun <T : Any> Value<T>.useAsState(): StateInstance<T> {
    val state = useState { value }
    val (_, set) = state

    useEffectOnceWithCleanup {
        val observer: (T) -> Unit = { set(it) }
        val subscription = subscribe(observer)
        onCleanup { subscription.cancel() }
    }

    return state
}