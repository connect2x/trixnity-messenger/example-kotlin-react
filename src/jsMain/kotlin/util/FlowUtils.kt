package de.connect2x.util

import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import react.StateInstance
import react.useEffectOnceWithCleanup
import react.useState

private val log = KotlinLogging.logger { }

fun <T> StateFlow<T>.collectAsState(): StateInstance<T> {
    val state = useState { value }

    useAsyncEffect {
        this@collectAsState.collect {
            log.debug { "set value of $this: $it" }
            state.component2().invoke(it)
        }
    }

    return state
}

@OptIn(DelicateCoroutinesApi::class)
fun useAsyncEffect(
    block: suspend () -> Unit,
) {
    useEffectOnceWithCleanup {
        val job = GlobalScope.launch {
            block()
        }

        onCleanup { job.cancel() }
    }
}
