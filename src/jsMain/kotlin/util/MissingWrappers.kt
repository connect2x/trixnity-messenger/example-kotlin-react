package de.connect2x.util

import mui.base.InputBaseProps
import mui.base.SliderProps
import mui.base.TabsProps
import mui.material.GridProps
import mui.material.ListItemProps
import mui.material.TextFieldProps
import mui.material.TypographyProps
import mui.system.BoxProps
import web.cssom.Display

// @see https://github.com/karakum-team/kotlin-mui-showcase/blob/main/src/jsMain/kotlin/team/karakum/MissedWrappers.kt

inline var GridProps.xs: Int
    get() = TODO("Prop is write-only!")
    set(value) {
        asDynamic().xs = value
    }

inline var TypographyProps.color: String
    get() = TODO("Prop is write-only!")
    set(value) {
        asDynamic().color = value
    }

inline var BoxProps.noValidate: Boolean
    get() = TODO("Prop is write-only!")
    set(value) {
        asDynamic().noValidate = value
    }

inline var BoxProps.autoComplete: String
    get() = TODO("Prop is write-only!")
    set(value) {
        asDynamic().autoComplete = value
    }

inline var TabsProps.ariaLabel: String
    get() = TODO("Prop is write-only!")
    set(value) {
        asDynamic()["aria-label"] = value
    }
inline var SliderProps.ariaLabel: String
    get() = TODO("Prop is write-only!")
    set(value) {
        asDynamic()["aria-label"] = value
    }

inline var TextFieldProps.InputProps: InputBaseProps
    get() = TODO("Prop is write-only!")
    set(value) {
        asDynamic().InputProps = value
    }

inline var ListItemProps.button: Boolean
    get() = TODO("Prop is write-only!")
    set(value) {
        asDynamic().button = value
    }

inline var TypographyProps.display: Display
    get() = TODO("Prop is write-only!")
    set(value) {
        asDynamic().display = value
    }