package de.connect2x.messenger

import de.connect2x.trixnity.messenger.viewmodel.MainViewModel
import de.connect2x.trixnity.messenger.viewmodel.roomlist.RoomListRouter
import de.connect2x.util.useAsState
import react.FC
import react.Props

external interface MainViewProps : Props {
    var viewModel: MainViewModel
}

val MainView = FC<MainViewProps> { props ->
    val (roomListStack) = props.viewModel.roomListRouterStack.useAsState()
    when (val roomListChild = roomListStack.active.instance) {
        is RoomListRouter.Wrapper.None -> {}
        is RoomListRouter.Wrapper.List -> RoomListView { viewModel = roomListChild.viewModel }
        else -> +"unknown RoomListState"
    }
}