package de.connect2x.messenger

import de.connect2x.trixnity.messenger.viewmodel.roomlist.RoomListElementViewModel
import de.connect2x.trixnity.messenger.viewmodel.roomlist.RoomListViewModel
import de.connect2x.util.collectAsState
import io.ktor.util.*
import mui.material.Avatar
import mui.material.Typography
import mui.material.styles.TypographyVariant
import react.FC
import react.Props

external interface RoomListViewProps : Props {
    var viewModel: RoomListViewModel
}

val RoomListView = FC<RoomListViewProps> { props ->
    val (roomListElementViewModels) = props.viewModel.elements.collectAsState()

    Typography {
        variant = TypographyVariant.h3
        +"RoomList (${roomListElementViewModels.size})"
    }
    roomListElementViewModels.map { roomListElementViewModel ->
        RoomListElementView { viewModel = roomListElementViewModel }
    }
}

external interface RoomListElementViewProps : Props {
    var viewModel: RoomListElementViewModel
}

val RoomListElementView = FC<RoomListElementViewProps> { props ->
    val (roomName) = props.viewModel.roomName.collectAsState()
    val (roomImage) = props.viewModel.roomImage.collectAsState()
    val base64Avatar = roomImage?.encodeBase64()
    Avatar {
        src = "data:image/png;base64,$base64Avatar"
    }
    Typography {
        variant = TypographyVariant.body1
        +roomName
    }
}