package de.connect2x

import de.connect2x.connecting.AddMatrixAccountView
import de.connect2x.connecting.PasswordLoginView
import de.connect2x.messenger.MainView
import de.connect2x.trixnity.messenger.createRoot
import de.connect2x.trixnity.messenger.multi.MatrixMultiMessenger
import de.connect2x.trixnity.messenger.multi.create
import de.connect2x.trixnity.messenger.multi.singleMode
import de.connect2x.trixnity.messenger.viewmodel.RootRouter
import de.connect2x.trixnity.messenger.viewmodel.RootViewModel
import de.connect2x.util.useAsState
import io.github.oshai.kotlinlogging.KotlinLogging
import react.FC
import react.Props
import react.create
import react.dom.client.createRoot
import web.dom.document

private val log = KotlinLogging.logger { }

suspend fun main() {
    val container = document.getElementById("root") ?: error("Couldn't find root container!")
    val matrixMultiMessenger = MatrixMultiMessenger.create(configuration = messengerConfig)

    log.info { "Created MatrixMultiMessenger" }
    matrixMultiMessenger.singleMode { matrixMessenger ->
        val rootViewModel = matrixMessenger.createRoot()
        createRoot(container).render(App.create { viewModel = rootViewModel })
    }
}

external interface RootViewModelProps : Props {
    var viewModel: RootViewModel
}

private val App = FC<RootViewModelProps> { props ->
    val (stack) = props.viewModel.stack.useAsState()
    when (val child = stack.active.instance) {
        is RootRouter.Wrapper.AddMatrixAccount -> AddMatrixAccountView { viewModel = child.viewModel }
        is RootRouter.Wrapper.PasswordLogin -> PasswordLoginView { viewModel = child.viewModel }
        is RootRouter.Wrapper.Main -> MainView { viewModel = child.viewModel }

        else -> +"unknown state"
    }
}