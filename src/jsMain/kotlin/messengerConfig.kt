package de.connect2x

import de.connect2x.trixnity.messenger.multi.MatrixMultiMessengerConfiguration

val messengerConfig: MatrixMultiMessengerConfiguration.() -> Unit = {
    appId = "de.connect2x.trixnity.messenger.compose.example"
    appName = "Trixnity-Messenger-Example-Web"

    messenger = {
        // colors are used when multiple accounts are used within the same profile
        val colors =
            listOf(
                0xAAB749C7,
                0xAAC77849,
                0xAA59C749,
            )
        generateInitialAccountColor = { alreadyUsedColors: Set<Long> ->
            colors.firstOrNull { alreadyUsedColors.contains(it).not() } ?: 0xFF333333
        }

        modulesFactories += emptyList() // add your own modules here
    }
}