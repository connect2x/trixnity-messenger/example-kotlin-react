package de.connect2x.connecting

import de.connect2x.trixnity.messenger.viewmodel.connecting.AddMatrixAccountState
import de.connect2x.trixnity.messenger.viewmodel.connecting.PasswordLoginViewModel
import de.connect2x.util.collectAsState
import de.connect2x.util.xs
import mui.material.Button
import mui.material.ButtonVariant.Companion.contained
import mui.material.ButtonVariant.Companion.outlined
import mui.material.CircularProgress
import mui.material.Grid
import mui.material.OutlinedTextFieldProps
import mui.material.TextField
import mui.material.Typography
import mui.material.styles.TypographyVariant
import mui.system.responsive
import react.FC
import react.Props
import react.ReactNode
import web.html.HTMLInputElement
import web.html.InputType

external interface PasswordLoginViewProps : Props {
    var viewModel: PasswordLoginViewModel
}

val PasswordLoginView = FC<PasswordLoginViewProps> { props ->
    val (username) = props.viewModel.username.collectAsState()
    val (password) = props.viewModel.password.collectAsState()
    val (canLogin) = props.viewModel.canLogin.collectAsState()
    val (addMatrixAccountState) = props.viewModel.addMatrixAccountState.collectAsState()

    Grid {
        container = true
        spacing = responsive(2)

        Grid {
            item = true
            xs = 12
            TextField<OutlinedTextFieldProps> {
                label = ReactNode("Username")
                value = username
                onChange =
                    { event ->
                        props.viewModel.username.update((event.target as? HTMLInputElement)?.value ?: "")
                    }
            }
            Grid {
                item = true
                xs = 12
                TextField<OutlinedTextFieldProps> {
                    label = ReactNode("Password")
                    value = password
                    onChange =
                        { event ->
                            props.viewModel.password.update((event.target as? HTMLInputElement)?.value ?: "")
                            type = InputType.password
                        }
                }
                Grid {
                    item = true
                    xs = 12
                    when (addMatrixAccountState) {
                        is AddMatrixAccountState.None -> {}
                        is AddMatrixAccountState.Success -> {}
                        is AddMatrixAccountState.Connecting -> CircularProgress
                        is AddMatrixAccountState.Failure -> Typography {
                            variant = TypographyVariant.body2
                            +addMatrixAccountState.message
                        }
                    }
                }
                Grid {
                    item = true
                    xs = 2
                    Button {
                        variant = outlined
                        +"Cancel"
                        onClick = { props.viewModel.back() }
                    }
                }
                Grid {
                    item = true
                    xs = 2
                    Button {
                        variant = contained
                        +"Login"
                        disabled = canLogin.not()
                        onClick = { props.viewModel.tryLogin() }
                    }
                }
                Grid {
                    item = true
                    xs = 8
                }
            }
        }
    }
}