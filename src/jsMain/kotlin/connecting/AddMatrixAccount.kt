package de.connect2x.connecting

import de.connect2x.trixnity.messenger.viewmodel.connecting.AddMatrixAccountMethod
import de.connect2x.trixnity.messenger.viewmodel.connecting.AddMatrixAccountViewModel
import de.connect2x.util.collectAsState
import de.connect2x.util.xs
import io.github.oshai.kotlinlogging.KotlinLogging
import mui.material.Button
import mui.material.ButtonVariant.Companion.contained
import mui.material.CircularProgress
import mui.material.Grid
import mui.material.StandardTextFieldProps
import mui.material.TextField
import mui.system.responsive
import react.FC
import react.Props
import react.ReactNode
import web.html.HTMLInputElement

private val log = KotlinLogging.logger { }

external interface AddMatrixAccountProps : Props {
    var viewModel: AddMatrixAccountViewModel
}

val AddMatrixAccountView = FC<AddMatrixAccountProps> { props ->
    val (serverUrl) = props.viewModel.serverUrl.collectAsState()
    val (discoveryState) = props.viewModel.serverDiscoveryState.collectAsState()
    Grid {
        container = true
        spacing = responsive(2)

        Grid {
            item = true
            xs = 12
            TextField<StandardTextFieldProps> {
                value = serverUrl
                onChange =
                    { event -> props.viewModel.serverUrl.update((event.target as? HTMLInputElement)?.value ?: "") }
                label = ReactNode("Matrix-Server")
            }
        }
        Grid {
            item = true
            xs = 12

            when (discoveryState) {
                is AddMatrixAccountViewModel.ServerDiscoveryState.Success -> AddMatrixAccountMethodsView {
                    viewModel = props.viewModel
                    methods = discoveryState.addMatrixAccountMethods
                }

                is AddMatrixAccountViewModel.ServerDiscoveryState.Loading -> CircularProgress {}
                is AddMatrixAccountViewModel.ServerDiscoveryState.Failure -> +"failure: ${discoveryState.message}"
                is AddMatrixAccountViewModel.ServerDiscoveryState.None -> {}
            }
        }
    }
}

external interface AddMatrixAccountMethodsProps : Props {
    var viewModel: AddMatrixAccountViewModel
    var methods: Set<AddMatrixAccountMethod>
}

val AddMatrixAccountMethodsView = FC<AddMatrixAccountMethodsProps> { props ->
    Grid {
        container = true
        item = true
        spacing = responsive(2)

        props.methods.map { method ->
            Grid {
                item = true
                xs = 12
                Button {
                    variant = contained
                    when (method) {
                        is AddMatrixAccountMethod.Password -> +"Login with password"
                        is AddMatrixAccountMethod.SSO -> +method.identityProvider?.name
                        is AddMatrixAccountMethod.Register -> +"Register a new account"
                    }
                    onClick = {
                        props.viewModel.selectAddMatrixAccountMethod(method)
                    }
                }
            }
        }
    }
}